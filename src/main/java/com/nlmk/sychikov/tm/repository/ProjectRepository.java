package com.nlmk.sychikov.tm.repository;

import com.nlmk.sychikov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Projects storage
 */
public class ProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        return project;
    }

    public int getRepositorySize() {
        return projects.size();
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public void clear() {
        projects.clear();
    }

    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    public Project findByName(final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public List<Project> findAll() {
        return projects;
    }

}

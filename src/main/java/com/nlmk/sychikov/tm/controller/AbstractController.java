package com.nlmk.sychikov.tm.controller;

import java.util.Scanner;

public abstract class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

    /**
     * Index input processing with error message when not a number or empty value
     * or negative value received
     *
     * @return entered int value or -1 (when input is wrong)
     */
    protected int indexInputProcessor(final String inputPrompt) {
        System.out.print(inputPrompt);
        final String stringValue = scanner.nextLine();
        try {
            final int i = Integer.parseInt(stringValue);
            if (i > 0)
                return i - 1;
            else
                System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return -1;
        } catch (NumberFormatException e) {
            System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return -1;
        }
    }

    /**
     * Id Long input processing with error message when not a number or empty value
     * or negative value received
     *
     * @return entered Long value or -1 (when input is wrong)
     */
    protected Long idInputProcessor(final String inputPrompt) {
        System.out.print(inputPrompt);
        final String stringValue = scanner.nextLine();
        try {
            final long l = Long.parseLong(stringValue);
            if (l > 0)
                return l;
            else
                System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return (long) -1;
        } catch (NumberFormatException e) {
            System.out.println("WRONG INPUT VALUE '" + stringValue + "'! COMMAND TERMINATED.");
            return (long) -1;
        }
    }

}

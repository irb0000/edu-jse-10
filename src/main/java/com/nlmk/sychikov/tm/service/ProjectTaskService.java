package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.entity.Task;
import com.nlmk.sychikov.tm.repository.ProjectRepository;
import com.nlmk.sychikov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findById(projectId);
        if (projectId == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        if (projectId == null || taskId == null) return null;
        return taskRepository.removeFromProjectByIds(projectId, taskId);
    }

    public Project removeProjectById(final Long projectId) {
        if (projectId == null) return null;
        for (final Task task : findAllByProjectId(projectId)) {
            taskRepository.removeById(task.getId());
        }
        return projectRepository.removeById(projectId);
    }

    public Project removeProjectByIndex(final int projectIndex) {
        if (projectIndex > projectRepository.getRepositorySize() - 1 || projectIndex < 0) return null;
        final Long projectId = projectRepository.findByIndex(projectIndex).getId();
        return removeProjectById(projectId);
    }

    public Project removeProjectByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Long projectId = projectRepository.findByName(name).getId();
        return removeProjectById(projectId);
    }

}
